package com.learnmore.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.learnmore.entity.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Long>{
	
    Optional<Users> findByEmail(String email);

    Boolean existsByEmail(String email);
	@Query("SELECT user FROM Users user WHERE user.email = ?1")
    Users getByEmail(String email);
}
