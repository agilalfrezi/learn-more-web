package com.learnmore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.learnmore.entity.Article;
import com.learnmore.entity.Category;
import com.learnmore.entity.Users;

@Repository
public interface ArticleRepostitory extends JpaRepository<Article, Long> {
	@Query("SELECT art FROM Article art WHERE art.id = ?1")
	public Article getArticle(Long id);

	public Article findByIdAndUsers(Users users, Long id);

	public Article findByTitle(String title);

	Page<Article> findByCategory(Category category, Pageable pageable);

	@Query("SELECT art FROM Article art JOIN art.category category WHERE "
			+ "category.categoryName =: p_categoryOne AND category.categoryName =: p_categoryTwo")
	Page<Article> findByCategory(String categoryOne, String CategoryTwo, Pageable pageable);

	Page<Article> findByUsers(Users users, Pageable pageable);

	public Article findByIdAndUsers(Long id, Users users);
}
