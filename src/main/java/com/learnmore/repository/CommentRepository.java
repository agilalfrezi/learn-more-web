package com.learnmore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.learnmore.entity.Article;
import com.learnmore.entity.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {
	public Page<Comment> findByArticle(Article article, Pageable pageable);
}
