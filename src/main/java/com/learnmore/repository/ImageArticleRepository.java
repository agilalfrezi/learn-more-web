package com.learnmore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.learnmore.entity.Article;
import com.learnmore.entity.ImageArticle;

@Repository
public interface ImageArticleRepository extends JpaRepository<ImageArticle, Long> {
	public ImageArticle findByFileCode(String filecode);

	public ImageArticle findByArticle(Article article);
}
