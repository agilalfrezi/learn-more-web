package com.learnmore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learnmore.entity.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {
	public Category findByCategoryName(String categoryName);
}
