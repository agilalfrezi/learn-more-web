package com.learnmore.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.learnmore.entity.Comment;
import com.learnmore.entity.CommentReply;

@Repository
public interface CommentReplyRepository extends JpaRepository<CommentReply, Long> {
	public Page<CommentReply> findByComment(Comment comment, Pageable pageable);
}
