package com.learnmore.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.learnmore.entity.UserImage;
import com.learnmore.entity.Users;

public interface UserImageRepository extends JpaRepository<UserImage, Long> {
	UserImage findByFileCode(String fileCode);

	UserImage findByUsers(Users users);
}
