package com.learnmore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.learnmore.entity.SystemParam;

@Repository
public interface SystemParamRepository extends JpaRepository<SystemParam, Long>{
	public SystemParam findByParamName(String paramValue);
}
