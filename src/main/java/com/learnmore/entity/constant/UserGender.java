package com.learnmore.entity.constant;

public enum UserGender {
	MALE("MALE"),FEMALE("FEMALE");
	
	
	private UserGender(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	private String value;

}
