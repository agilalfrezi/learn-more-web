package com.learnmore.entity.constant;

public enum  AuthProvider {
    local,
    facebook,
    google,
    github
}