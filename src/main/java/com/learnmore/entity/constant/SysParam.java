package com.learnmore.entity.constant;

public enum SysParam {
	DIG_URL("DIG-URL"), IMAGE_DIR("IMAGE_DIR"), IMAGE_CONTENT("IMAGE_CONTENT"), USR_IMAGE_DIR("USR_IMAGE_DIR"),
	ARTICLE_IMAGE_URL("/article-image/"),CONTENT_IMAGE_URL("/image-content/");

	private SysParam(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}
}
