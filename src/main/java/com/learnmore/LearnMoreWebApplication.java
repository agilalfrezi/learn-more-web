package com.learnmore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.learnmore.security.config.AppProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class LearnMoreWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnMoreWebApplication.class, args);
	}

}
