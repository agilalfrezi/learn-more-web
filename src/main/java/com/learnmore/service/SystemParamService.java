package com.learnmore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learnmore.entity.SystemParam;
import com.learnmore.repository.SystemParamRepository;

@Service
public class SystemParamService {
	@Autowired
	private SystemParamRepository systemParamRepository;

	public SystemParam getSystemParamName(String name) {
		return systemParamRepository.findByParamName(name);
	}

	public void saveSystemParam(SystemParam systemParam) {
		systemParamRepository.save(systemParam);
	}
}
