package com.learnmore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learnmore.entity.Article;
import com.learnmore.entity.ImageArticle;
import com.learnmore.entity.UserImage;
import com.learnmore.entity.Users;
import com.learnmore.repository.ImageArticleRepository;
import com.learnmore.repository.UserImageRepository;

@Service
public class ImageService {
	@Autowired
	private ImageArticleRepository imageArticleRepository;
	@Autowired
	private UserImageRepository userImageRepository;

	public ImageArticle findByFilecode(String filecode) {
		return imageArticleRepository.findByFileCode(filecode);
	}

	public void saveImage(ImageArticle imageArticle) {
		imageArticleRepository.save(imageArticle);
	}

	public void deleteImage(ImageArticle imageArticle) {
		imageArticleRepository.delete(imageArticle);
	}

	public void saveImageUser(UserImage userImage) {
		userImageRepository.save(userImage);
	}

	public UserImage findUserImageByFilecode(String filecode) {
		return userImageRepository.findByFileCode(filecode);
	}

	public UserImage findByUserImage(Users users) {
		return userImageRepository.findByUsers(users);
	}

	public ImageArticle findByArticle(Article article) {
		return imageArticleRepository.findByArticle(article);
	}
}
