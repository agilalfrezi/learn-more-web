package com.learnmore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.learnmore.entity.Article;
import com.learnmore.entity.Comment;
import com.learnmore.entity.CommentReply;
import com.learnmore.repository.CommentReplyRepository;
import com.learnmore.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private CommentReplyRepository commentReplyRepository;

	public Page<Comment> pageComment() {
		Pageable pageable = PageRequest.of(0, 3000);
		return commentRepository.findAll(pageable);
	}

	public Comment getById(Long id) {
		return commentRepository.getOne(id);
	}

	public Page<Comment> getCommentByArticle(Article article) {
		Pageable pageable = PageRequest.of(0, 3000);
		return commentRepository.findByArticle(article, pageable);
	}

	public Page<CommentReply> getReplyCommentAll() {
		Pageable pageable = PageRequest.of(0, 300);
		return commentReplyRepository.findAll(pageable);
	}

	public Page<CommentReply> getByComment(Comment comment) {
		Pageable pageable = PageRequest.of(0, 300);
		return commentReplyRepository.findByComment(comment, pageable);
	}

	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}

	public void saveCommentReply(CommentReply commentReply) {
		commentReplyRepository.save(commentReply);
	}
}
