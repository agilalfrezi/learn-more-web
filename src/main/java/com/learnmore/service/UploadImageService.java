package com.learnmore.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.learnmore.entity.Article;
import com.learnmore.entity.ImageArticle;
import com.learnmore.entity.SystemParam;
import com.learnmore.entity.constant.SysParam;

@Service
@ConfigurationProperties(prefix = "upload.file.path")
public class UploadImageService {

	@Autowired
	private SystemParamService systemParamService;

	public String saveUploadFile(MultipartFile multipartFile, String uri) throws IOException {
		SystemParam baseImageDir = systemParamService.getSystemParamName("IMAGE_CONTENT");
		String imageDir = baseImageDir.getParamValue() + File.separator;
		String originalFilename = multipartFile.getOriginalFilename();
		String extensionName = originalFilename.substring(originalFilename.lastIndexOf("."));
		String saveFileName = LocalDateTime.now().toString("yyyyMMddHHmmssSSS") + Thread.currentThread().getId()
				+ extensionName;
		Path path = Paths.get(imageDir, saveFileName);
		Files.write(path, multipartFile.getBytes());
		String fileUrl = uri + SysParam.CONTENT_IMAGE_URL + saveFileName;
		System.err.println(fileUrl);

		return fileUrl;
	}

	public ImageArticle saveThumbnail(MultipartFile multipartFile, Article article) {
		SystemParam baseImageDir = systemParamService.getSystemParamName(SysParam.IMAGE_DIR.getValue());
		byte[] imageBytes = null;
		try {
			imageBytes = multipartFile.getBytes();
		} catch (IOException ee) {
			ee.printStackTrace();
		}
		File newFilename = new File(UUID.randomUUID().toString().toLowerCase() + ".jpeg");
		String imageDir = baseImageDir.getParamValue() + File.separator + "ARTIKEL" + File.separator
				+ article.getParamUrl();
		File uploadDir = new File(imageDir);
		if (!uploadDir.exists()) {
			uploadDir.mkdirs();
		}

		try {
			Path path = Paths.get(uploadDir + File.separator + newFilename);
			Files.write(path, imageBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String fileCode = UUID.randomUUID().toString().toLowerCase().replace("-", "");
		ImageArticle image = new ImageArticle();
		image.setContentType("image/jpeg");
		image.setFileFolder(imageDir);
		image.setFileName(newFilename.toString());
		image.setFileCode(fileCode);
		image.setArticle(article);
		return image;
	}
}
