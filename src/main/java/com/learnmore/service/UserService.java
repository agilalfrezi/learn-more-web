package com.learnmore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.learnmore.DTO.PrincipalDTO;
import com.learnmore.entity.Users;
import com.learnmore.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public Users findById(Long id) {
		return userRepository.getOne(id);
	}

	public void saveUser(Users user) {
		userRepository.save(user);
	}

	public Page<Users> getAll() {
		Pageable pageable = PageRequest.of(0, 3000);
		return userRepository.findAll(pageable);
	}

	public Users findByEmail(String email) {
		return userRepository.getByEmail(email);
	}

	public Users dtoToUsers(PrincipalDTO principalDTO) {
		Users users = findById(principalDTO.getId());
		return users;
	}
}
