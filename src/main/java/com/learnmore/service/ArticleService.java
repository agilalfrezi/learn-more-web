package com.learnmore.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.learnmore.DTO.ArticleDTO;
import com.learnmore.entity.Article;
import com.learnmore.entity.Category;
import com.learnmore.entity.ImageArticle;
import com.learnmore.entity.Users;
import com.learnmore.repository.ArticleRepostitory;
import com.learnmore.utils.CommonUtils;

@Service
public class ArticleService {

	@Autowired
	private ArticleRepostitory articleRepostitory;
	@Autowired
	private ImageService imageService;

	public List<Article> getAll() {
		return articleRepostitory.findAll();
	}

	public Article findArticleById(Long id) {
		return articleRepostitory.getArticle(id);
	}

	public Article findArticleByIdAndUser(Users users, Long id) {
		return articleRepostitory.findByIdAndUsers(id, users);
	}

	public Article getArticleByTitle(String title) {
		return articleRepostitory.findByTitle(title);
	}

	public void saveArticle(Article article) {
		articleRepostitory.save(article);
	}

	public Page<Article> getRecentArticle(int page) {
		Pageable pageable = PageRequest.of(page, 4, Direction.DESC, "id");
		return articleRepostitory.findAll(pageable);
	}

	public Page<Article> findByCategory(Category category) {
		Pageable pageable = PageRequest.of(0, 3);
		return articleRepostitory.findByCategory(category, pageable);
	}

	public Page<Article> findArticleByCategoryMoreParameter(String categoryOne, String categoryTwo) {
		Pageable pageable = PageRequest.of(0, 3);
		return articleRepostitory.findByCategory(categoryOne, categoryTwo, pageable);
	}

	public Page<Article> findByUser(Users users) {
		Pageable pageable = PageRequest.of(0, 4);
		return articleRepostitory.findByUsers(users, pageable);
	}

	public Article findArticleByIdAndUsers(Long id, Users users) {
		return articleRepostitory.findByIdAndUsers(id, users);
	}

	public void deleteArticle(Article article) {
		articleRepostitory.delete(article);
	}

	public List<ArticleDTO> toDTOAricle(Page<Article> listPage) {

		List<ArticleDTO> list = new ArrayList<>();
		for (Article article : listPage) {
			ImageArticle imageArticle = imageService.findByArticle(article);
			ArticleDTO articleDTO = new ArticleDTO();
			articleDTO.setId(article.getId());
			articleDTO.setAuthor(article.getUsers().getName());
			articleDTO.setCategory(article.getCategory().getCategoryName());
			articleDTO.setDate(article.getCreatedDate().toString());
			articleDTO.setSeen(String.valueOf(article.getSeen()));
			articleDTO.setTitle(article.getTitle());
			articleDTO.setTotalComment("4");
			articleDTO.setImageUrl("/article-image/" + imageArticle.getFileCode());
			articleDTO.setParamUrl(article.getParamUrl());
			list.add(articleDTO);
		}

		return list;
	}

	public ArticleDTO toDtoArticle(Article article) {

		ArticleDTO articleDTO = new ArticleDTO();
		ImageArticle imageArticle = imageService.findByArticle(article);
		String date = CommonUtils.date(article.getCreatedDate());

		articleDTO.setContent(article.getContent());
		articleDTO.setId(article.getId());
		articleDTO.setTitle(article.getTitle());
		articleDTO.setCategory(article.getCategory().getCategoryName());
		articleDTO.setDate(date);
		articleDTO.setAuthor(article.getUsers().getName());
		articleDTO.setSeen(String.valueOf(article.getSeen()));
		articleDTO.setTotalComment("3");
		articleDTO.setImageUrl("/article-image/" + imageArticle.getFileCode());
		articleDTO.setParamUrl(article.getParamUrl());
		return articleDTO;
	}
}
