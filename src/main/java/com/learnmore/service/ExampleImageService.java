package com.learnmore.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learnmore.entity.ImageArticle;
import com.learnmore.entity.SystemParam;
import com.learnmore.entity.UserImage;

@Service
public class ExampleImageService {
	
	
	@Autowired
	private SystemParamService systemParamService;
	public ImageArticle initImageArticle(String imageUrl) {

		SystemParam baseImageDir = systemParamService.getSystemParamName("IMAGE_DIR");

		byte[] imageBytes = null;
		URL url = null;
		try {
			url = new URL(imageUrl);
			BufferedImage bufferimage = ImageIO.read(url);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			ImageIO.write(bufferimage, "jpg", output);
			byte[] data = output.toByteArray();
			imageBytes = data;
		} catch (IOException ee) {
			ee.printStackTrace();
		}
		File newFilename = new File(UUID.randomUUID().toString().toLowerCase() + ".jpeg");
		String imageDir = baseImageDir.getParamValue() + File.separator + "ARTIKEL";
		File uploadDir = new File(imageDir);
		if (!uploadDir.exists()) {
			uploadDir.mkdirs();
		}

		try {
			Path path = Paths.get(uploadDir + File.separator + newFilename);
			Files.write(path, imageBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String fileCode = UUID.randomUUID().toString().toLowerCase().replace("-", "");
		ImageArticle image = new ImageArticle();
		image.setContentType("image/jpeg");
		image.setFileFolder(imageDir);
		image.setFileName(newFilename.toString());
		image.setFileCode(fileCode);
		return image;
	}
	
	public UserImage initImage(String imageUrl, String userEmail) {
		SystemParam baseImageDir = systemParamService.getSystemParamName("USR_IMAGE_DIR");
		byte[] imageBytes = null;
		URL url = null;
		try {
			url = new URL(imageUrl);
			BufferedImage bufferimage = ImageIO.read(url);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			ImageIO.write(bufferimage, "jpg", output);
			byte[] data = output.toByteArray();
			imageBytes = data;
		} catch (IOException ee) {
			ee.printStackTrace();
		}
		File newFilename = new File(UUID.randomUUID().toString().toLowerCase() + ".jpeg");
		String imageDir = baseImageDir.getParamValue() + File.separator + "USER_IMAGE" + userEmail;
		File uploadDir = new File(imageDir);
		if (!uploadDir.exists()) {
			uploadDir.mkdirs();
		}

		try {
			Path path = Paths.get(uploadDir + File.separator + newFilename);
			Files.write(path, imageBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String fileCode = UUID.randomUUID().toString().toLowerCase().replace("-", "");
		UserImage image = new UserImage();
		image.setContentType("image/jpeg");
		image.setFileFolder(imageDir);
		image.setFileName(newFilename.toString());
		image.setFileCode(fileCode);
		return image;
	}
}
