package com.learnmore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.learnmore.entity.Category;
import com.learnmore.repository.CategoryRepository;

@Service
public class CategoryService {
	@Autowired
	private CategoryRepository categoryRepository;

	public List<Category> getAll() {
		return categoryRepository.findAll();
	}

	public Category getCategoryByName(String categoryName) {
		return categoryRepository.findByCategoryName(categoryName);
	}
	
	public Category getCategoryById(Long id) {
		return categoryRepository.getOne(id);
	}
	
	public void saveCategory(Category category) {
		categoryRepository.save(category);
	}
}
