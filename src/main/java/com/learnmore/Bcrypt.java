package com.learnmore;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.learnmore.entity.UserImage;

public class Bcrypt {
	

	public static void main(String[] args) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		String pass = "123456789";
		String encode = bCryptPasswordEncoder.encode(pass);
		System.out.println(encode);
	}
	
	public static UserImage initImage() {

		byte[] imageBytes = null;
		URL url = null;
		try {
			url = new URL("https://lh4.googleusercontent.com/-8reox7YnmjE/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rdRqkRdu1VWzhpzyFg-GR6HpmcMVg/photo.jpg");
			BufferedImage bufferimage = ImageIO.read(url);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			ImageIO.write(bufferimage, "jpg", output);
			byte[] data = output.toByteArray();
			imageBytes = data;
		} catch (IOException ee) {
			ee.printStackTrace();
		}
		File newFilename = new File(UUID.randomUUID().toString().toLowerCase() + ".jpeg");
		String imageDir = "C:\\development\\upload-image-user" + File.separator + "USER_IMAGE";
		File uploadDir = new File(imageDir);
		if (!uploadDir.exists()) {
			uploadDir.mkdirs();
		}

		try {
			Path path = Paths.get(uploadDir + File.separator + newFilename);
			Files.write(path, imageBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String fileCode = UUID.randomUUID().toString().toLowerCase().replace("-", "");
		UserImage image = new UserImage();
		image.setContentType("image/jpeg");
		image.setFileFolder(imageDir);
		image.setFileName(newFilename.toString());
		image.setFileCode(fileCode);
		return image;
	}
}
