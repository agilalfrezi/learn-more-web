package com.learnmore.Controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.learnmore.DTO.ArticleDTO;
import com.learnmore.DTO.PrincipalDTO;
import com.learnmore.DTO.UserDTO;
import com.learnmore.entity.Article;
import com.learnmore.entity.Category;
import com.learnmore.entity.ImageArticle;
import com.learnmore.entity.SystemParam;
import com.learnmore.entity.UserImage;
import com.learnmore.entity.Users;
import com.learnmore.entity.constant.SysParam;
import com.learnmore.service.ArticleService;
import com.learnmore.service.CategoryService;
import com.learnmore.service.ImageService;
import com.learnmore.service.SystemParamService;
import com.learnmore.service.UploadImageService;
import com.learnmore.service.UserService;
import com.learnmore.utils.CommonUtils;

@Controller
public class UserController extends BaseController {

	@Autowired
	private UploadImageService uploadImageService;
	@Autowired
	private ArticleService articleService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private SystemParamService systemParamService;
	@Autowired
	private UserService userService;

	@RequestMapping("/user")
	public String dashboarUser(Model model, Principal principal) {

		Users users = userService.dtoToUsers(getUserLogin(principal));

		Page<Article> listArticle = articleService.findByUser(users);
		List<ArticleDTO> articleDTOs = articleService.toDTOAricle(listArticle);

		model.addAttribute("list_article", articleDTOs);
		return "menu/user/dashboard";
	}

	@RequestMapping("/user/create-article")
	public String userMenu(Model model) {

		model.addAttribute("list_category", categoryService.getAll());
		model.addAttribute("article", new Article());

		return "menu/user/article/create-article";
	}

	@RequestMapping("/user/edit-article")
	public String editArticle(Model model, @RequestParam("id") Long id, Principal principal) {

		Users users = userService.dtoToUsers(getUserLogin(principal));
		Article article = articleService.findArticleByIdAndUser(users, id);
		ImageArticle imageArticle = imageService.findByArticle(article);

		String imageUrl = SysParam.ARTICLE_IMAGE_URL + imageArticle.getFileCode();

		model.addAttribute("article", article);
		model.addAttribute("imageUrl", imageUrl);
		model.addAttribute("list_category", categoryService.getAll());
		return "menu/user/article/edit-article";
	}

	@PostMapping("/user/edit-article")
	public String saveEditArticle(@ModelAttribute("article") Article editArticle,
			@RequestParam("file") MultipartFile multipartFile, Principal principal) {

		String paramUrl = CommonUtils.removeSpace(editArticle.getTitle());

		SystemParam baseImageDir = systemParamService.getSystemParamName(SysParam.IMAGE_DIR.getValue());
		Category category = categoryService.getCategoryById(editArticle.getCategory().getId());

		Users user = userService.dtoToUsers(getUserLogin(principal));

		editArticle.setUpdatedDate(new Date());
		editArticle.setUsers(user);
		editArticle.setParamUrl(paramUrl);
		editArticle.setTitle(editArticle.getTitle());
		editArticle.setCategory(category);

		if (multipartFile.isEmpty()) {
			articleService.saveArticle(editArticle);
			return "redirect:/user";

		}

		String imageDir = baseImageDir.getParamValue() + File.separator + "ARTIKEL" + File.separator + paramUrl;
		File uploadDir = new File(imageDir);
		File[] listFiles = uploadDir.listFiles();
		for (File file : listFiles) {
			file.delete();
		}
		uploadDir.delete();
		articleService.saveArticle(editArticle);
		ImageArticle image = uploadImageService.saveThumbnail(multipartFile, editArticle);
		imageService.saveImage(image);

		return "redirect:/user";
	}

	@PostMapping("/user/create-article")
	public String addArticle(@ModelAttribute Article article, @RequestParam("file") MultipartFile multipartFile,
			RedirectAttributes model, Principal principal) {

		Users user = userService.dtoToUsers(getUserLogin(principal));
		List<String> errorMessages = new ArrayList<>();

		String paramUrl = CommonUtils.removeSpace(article.getTitle());

		if (!multipartFile.getContentType().equalsIgnoreCase("image/jpeg")
				&& !multipartFile.getContentType().equalsIgnoreCase("image/jpg")
				&& !multipartFile.getContentType().equalsIgnoreCase("image/png") && !multipartFile.isEmpty()) {
			errorMessages.add("File Can only accept jpeg/jpg/png");
		}

		if (!CommonUtils.isNotEmpty(article.getTitle()))
			errorMessages.add("Article Title cant be null.");

		if (!CommonUtils.isNotEmpty(article.getContent()))
			errorMessages.add("Article Content cant be null.");

		if (article.getCategory() == null)
			errorMessages.add("Article Category cant be null.");

		if (multipartFile.isEmpty())
			errorMessages.add("Article Thumbnail cant be null.");

		if (CommonUtils.isNotEmpty(errorMessages)) {
			model.addFlashAttribute("errorMessages", errorMessages);
			return "redirect:/user/create-article";
		}
		Category category = categoryService.getCategoryById(article.getCategory().getId());

		article.setUsers(user);
		article.setSeen(0);
		article.setCreatedDate(new Date());
		article.setUpdatedDate(new Date());
		article.setCategory(category);
		article.setParamUrl(paramUrl);
		articleService.saveArticle(article);
		ImageArticle image = uploadImageService.saveThumbnail(multipartFile, article);
		imageService.saveImage(image);
		return "redirect:/user";
	}

	@PostMapping("upload")
	public void upload(@RequestParam(value = "upload") MultipartFile multipartFile, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			String fileUrl = uploadImageService.saveUploadFile(multipartFile, request.getRequestURI());

			response.setContentType("text/html;charset=UTF-8");
			String callback = request.getParameter("CKEditorFuncNum");
			PrintWriter out = response.getWriter();
			out.println("<script type=\"text/javascript\">");
			out.println("window.parent.CKEDITOR.tools.callFunction(" + callback + ",'" + fileUrl + "',''" + ")");
			out.println("</script>");
			out.flush();
			out.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@RequestMapping("/user/profile")
	public String userProfile(Principal principal, Model model) {
		PrincipalDTO principalDTO = getUserLogin(principal);
		Users users = userService.findById(principalDTO.getId());
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = formatter.format(users.getDateOfBirth());
		String day = strDate.substring(0, 2);
		String month = strDate.substring(3, 5);
		String year = strDate.substring(6, 10);
		UserDTO userDTO = new UserDTO();
		userDTO = (UserDTO) convertToDTO(users, userDTO);
		userDTO.setDay(day);
		userDTO.setMonth(month);
		userDTO.setYear(year);

		model.addAttribute("user", userDTO);
		return "menu/user/profile";
	}

	@PostMapping("/user/profile/save")
	public String savePrfofile(@ModelAttribute("user") UserDTO userDTO,
			@RequestParam("file") MultipartFile multipartFile) throws ParseException {
		Users users = new Users();
		users = (Users) convertToEntity(userDTO, users);
		String date = userDTO.getDay() + "/" + userDTO.getMonth() + "/" + userDTO.getYear();
		Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(date);
		users.setDateOfBirth(date1);
		userService.saveUser(users);
		imageService.saveImageUser(saveUserPhoto(multipartFile, users));
		return "redirect:/user/profile";
	}

	public UserImage saveUserPhoto(MultipartFile multipartFile, Users users) {
		SystemParam baseImageDir = systemParamService.getSystemParamName("USR_IMAGE_DIR");
		byte[] imageBytes = null;
		try {
			imageBytes = multipartFile.getBytes();
		} catch (IOException ee) {
			ee.printStackTrace();
		}
		File newFilename = new File(UUID.randomUUID().toString().toLowerCase() + ".jpeg");
		String imageDir = baseImageDir.getParamValue() + File.separator + "USER_IMAGE" + users.getEmail();
		File uploadDir = new File(imageDir);
		if (!uploadDir.exists()) {
			uploadDir.mkdirs();
		}

		try {
			Path path = Paths.get(uploadDir + File.separator + newFilename);
			Files.write(path, imageBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String fileCode = UUID.randomUUID().toString().toLowerCase().replace("-", "");
		UserImage userImage = imageService.findByUserImage(users);
		userImage.setContentType("image/jpeg");
		userImage.setFileFolder(imageDir);
		userImage.setFileName(newFilename.toString());
		userImage.setFileCode(fileCode);
		return userImage;
	}

	public <T> T convertToEntity(T input, T output) {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		output = (T) modelMapper.map(input, output.getClass());
		return (T) output;
	}

	public <T> T convertToDTO(T input, T output) {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setAmbiguityIgnored(true);
		output = (T) modelMapper.map(input, output.getClass());
		return (T) output;
	}

//	@RequestMapping("/messaging")
//	public String message(Principal principal, Model model) {
//		PrincipalDTO userDTO = getUserLogin(principal);
//		Users users = userService.findById(userDTO.getId());
//		model.addAttribute("username", users.getName());
//		return "chat";
//	}
}
