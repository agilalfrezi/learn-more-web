package com.learnmore.Controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.learnmore.DTO.ArticleDTO;
import com.learnmore.DTO.CommentDTO;
import com.learnmore.entity.Article;
import com.learnmore.entity.Category;
import com.learnmore.entity.Comment;
import com.learnmore.entity.CommentReply;
import com.learnmore.service.ArticleService;
import com.learnmore.service.CategoryService;
import com.learnmore.service.CommentService;

@Controller
public class ArticleController extends BaseController {

	@Autowired
	private ArticleService articleService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private CommentService commentService;

	@GetMapping("/article-post/{id}/{paramUrl}")
	public String articlePost(Model model, @PathVariable("id") Long id, @PathVariable("paramUrl") String paramUrl,
			Principal principal) {

		Article article = articleService.findArticleById(id);

		// BUAT REPLY
		CommentDTO comment = new CommentDTO();
		comment.setArticleid(article.getId());

		ArticleDTO articleDTO = articleService.toDtoArticle(article);

		List<Category> listCategories = categoryService.getAll();

		List<CommentDTO> listCommentDTO = new ArrayList<CommentDTO>();
		Page<Comment> pagecomment = commentService.getCommentByArticle(article);
		Long totalComment = pagecomment.getTotalElements();
		for (Comment komentar : pagecomment) {
			CommentDTO commentDTO = new CommentDTO();
			commentDTO.setId(komentar.getId());
			commentDTO.setCommentMessage(komentar.getCommentMessage());
			commentDTO.setEmail(komentar.getEmail());
			commentDTO.setName(komentar.getName());
			commentDTO.setWebsite(komentar.getWebsite());
			List<CommentDTO> listreply = getListReply(komentar);
			totalComment += listreply.size();
			commentDTO.setListreply(listreply);
			listCommentDTO.add(commentDTO);
		}

		System.out.println(totalComment);

		model.addAttribute("totalComment", totalComment);
		model.addAttribute("listCategory", listCategories);
		model.addAttribute("article", articleDTO);
		model.addAttribute("list_comment", listCommentDTO);
		model.addAttribute("comment", comment);
		return "article-post";
	}

	@PostMapping("/article-post/comment")
	public String addComment(@ModelAttribute("comment") CommentDTO commentDTO) {

		Article article = articleService.findArticleById(commentDTO.getArticleid());

		Comment comment = new Comment();
		comment.setArticle(article);
		comment.setCommentMessage(commentDTO.getCommentMessage());
		comment.setEmail(commentDTO.getEmail());
		comment.setWebsite(commentDTO.getWebsite());
		comment.setName(commentDTO.getName());
		commentService.saveComment(comment);

		return "redirect:/article-post/" + article.getId() + "/" + article.getParamUrl();
	}

	private List<CommentDTO> getListReply(Comment comment) {
		List<CommentDTO> list = new ArrayList<CommentDTO>();
		Page<CommentReply> pagereply = commentService.getByComment(comment);
		for (CommentReply reply : pagereply) {
			CommentDTO dto = new CommentDTO();
			dto.setId(reply.getId());
			dto.setCommentMessage(reply.getCommentMessage());
			dto.setEmail(reply.getEmail());
			dto.setName(reply.getName());
			dto.setWebsite(reply.getWebsite());
			list.add(dto);
		}
		return list;
	}
}
