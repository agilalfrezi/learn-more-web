package com.learnmore.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.learnmore.DTO.ArticleDTO;
import com.learnmore.entity.Article;
import com.learnmore.entity.Category;
import com.learnmore.service.ArticleService;
import com.learnmore.service.CategoryService;

@Controller
public class IndexController {

	@Autowired
	private ArticleService articleService;
	@Autowired
	private CategoryService categoryService;

	@RequestMapping("/")
	public String index() {
		return "redirect:/home";
	}

	@RequestMapping("/home")
	public String index(Model model) {

		// GET RECENT ARTICLE
		Page<Article> pageArticle = articleService.getRecentArticle(0);
		List<ArticleDTO> dtoRecentArticle = articleService.toDTOAricle(pageArticle);

		// GET ALL CATEGORY
		List<Category> listCategories = categoryService.getAll();

		// ART TECH
		Category categoryTechnology = categoryService.getCategoryByName("TECHNOLOGY");
		Page<Article> pageArticleTechno = articleService.findByCategory(categoryTechnology);
		List<ArticleDTO> dtoArticleTechno = articleService.toDTOAricle(pageArticleTechno);

		// ART TUTORIAL
		Category categoryTutorial = categoryService.getCategoryByName("TUTORIAL");
		Page<Article> pageArticleTutorial = articleService.findByCategory(categoryTutorial);
		List<ArticleDTO> dtoArticleTutorial = articleService.toDTOAricle(pageArticleTutorial);

		// ART NEWS
		Category categoryNews = categoryService.getCategoryByName("NEWS");
		Page<Article> pageArticleNews = articleService.findByCategory(categoryNews);
		List<ArticleDTO> dtoArticleNews = articleService.toDTOAricle(pageArticleNews);

		model.addAttribute("listCategory", listCategories);
		model.addAttribute("listArticle", dtoRecentArticle);
		model.addAttribute("articleTech", dtoArticleTechno);
		model.addAttribute("newsArticle", dtoArticleNews);
		model.addAttribute("totalpage", pageArticle.getTotalPages());
		return "index";
	}

}
