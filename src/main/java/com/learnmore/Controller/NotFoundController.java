package com.learnmore.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NotFoundController {
	@RequestMapping("/404")
	public String pageNotFound() {
		return "404";
	}
}
