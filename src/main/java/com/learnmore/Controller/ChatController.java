package com.learnmore.Controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.learnmore.DTO.ChatDTO;
import com.learnmore.entity.UserImage;
import com.learnmore.entity.Users;
import com.learnmore.service.ImageService;
import com.learnmore.service.UserService;

@Controller
public class ChatController extends BaseController {

	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	@Autowired
	private UserService userService;
	@Autowired
	private ImageService imageService;

	@MessageMapping("/hello")
	@SendToUser("/queue/reply")
	public void chat(Principal principal, ChatDTO chatDTO) throws Exception {
		System.out.println(chatDTO.getToUser());
		System.out.println(chatDTO.getContent());

		Users users = userService.findByEmail(chatDTO.getFrom());
		UserImage userImage = imageService.findByUserImage(users);
		chatDTO.setImageReceiver("/user-image/" + userImage.getFileCode());
		//System.out.println(chatDTO.getImageReceiver());

		messagingTemplate.convertAndSendToUser(chatDTO.getToUser(), "/queue/reply", chatDTO);
	}

	@RequestMapping("/user/chat")
	public String chat(Model model, Principal principal) {
		Users users = userService.dtoToUsers(getUserLogin(principal));
		Page<Users> page = userService.getAll();
		model.addAttribute("list_user", page);
		model.addAttribute("username", users.getEmail());
		return "menu/user/chat";
	}
}
