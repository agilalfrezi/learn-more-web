package com.learnmore.Controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.learnmore.DTO.PrincipalDTO;
import com.learnmore.DTO.UserDTO;
import com.learnmore.entity.UserImage;
import com.learnmore.entity.Users;
import com.learnmore.service.ImageService;
import com.learnmore.service.UserService;

@Controller
public class AdminController extends BaseController {

	@Autowired
	private UserService userService;
	@Autowired
	private ImageService imageService;

	@RequestMapping("/admin/user-list")
	@PreAuthorize("hasAnyAuthority('ADMIN')")
	public String listUser(Principal principal, Model model) {
		PrincipalDTO principalDTO = getUserLogin(principal);
		Page<Users> listUsers = userService.getAll();
		List<UserDTO> userList = new ArrayList<>();

		for (Users users : listUsers) {
			UserDTO userDTO = new UserDTO();
			userDTO.setCreatedDate(users.getCreatedDate());
			userDTO.setEmail(users.getEmail());
			UserImage userImage = imageService.findByUserImage(users);
			userDTO.setImageUrl("/user-image/" + getFileCode(userImage));
			userDTO.setName(users.getName());
			userDTO.setRole(users.getRole());
			userDTO.setTotalPost("-");
			userDTO.setType(users.getProvider().toString());
			userList.add(userDTO);
		}
		model.addAttribute("list_user", userList);
		model.addAttribute("user", new UserDTO());
		return "menu/admin/list-user";
	}

	public String getFileCode(UserImage userImage) {
		String fileCode = userImage.getFileCode();
		return fileCode;
	}

	@PostMapping("/admin/user/change")
	public String change(@ModelAttribute("user") UserDTO userDTO) {
		System.out.println(userDTO.getRole());
		System.out.println(userDTO.getEmail());
		Users users = userService.findByEmail(userDTO.getEmail());
		users.setRole(userDTO.getRole().toUpperCase());
		userService.saveUser(users);
		return "redirect:/admin/user-list";
	}
}
