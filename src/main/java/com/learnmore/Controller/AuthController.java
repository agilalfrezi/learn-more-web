package com.learnmore.Controller;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.learnmore.DTO.RegisterDTO;
import com.learnmore.entity.Mail;
import com.learnmore.entity.Users;
import com.learnmore.entity.constant.AuthProvider;
import com.learnmore.exception.ResourceNotFoundException;
import com.learnmore.repository.UserRepository;
import com.learnmore.security.CurrentUser;
import com.learnmore.security.UserPrincipal;

@Controller
public class AuthController {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SpringTemplateEngine springTemplateEngine;

	@GetMapping("/user/me")
	@PreAuthorize("hasRole('USER')")
	public Users getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
		System.out.println(userPrincipal.getEmail());
		return userRepository.findById(userPrincipal.getId())
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));
	}

	@PostMapping("/signup")
	public String registerUser(@ModelAttribute RegisterDTO registerDTO, Model model, RedirectAttributes redirectAttributes,
			@RequestParam("gender") String gender) {
		System.out.println(gender);
		if (userRepository.existsByEmail(registerDTO.getEmail())) {
			System.err.println(true);
			redirectAttributes.addFlashAttribute("errorMessages", "user already exist");
			return "redirect:/login";
		}

		// Creating user's account
		Users user = new Users();
		user.setName(registerDTO.getName());
		user.setEmail(registerDTO.getEmail());
		user.setPassword(registerDTO.getPassword());
		user.setProvider(AuthProvider.local);
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setRole("USER");
		userRepository.save(user);

		Map<String, Object> map = new HashMap<String, Object>();
		String tbxone_url = "TEST";
		String link_activation = "TEST";
		map.put("link_activation", link_activation);
		map.put("companycode", tbxone_url);

		Context context = new Context();
		context.setVariables(map);
		String html = springTemplateEngine.process("register-activation", context);
		sendEmailActivation(registerDTO.getEmail(), html);

		return "redirect:/login";
	}

	public void sendEmailActivation(String emailTo, String html) {

		Mail mail = new Mail();
		mail.setFrom("agilalfrezi@gmail.com");
		mail.setTo(emailTo);
		mail.setSubject("Register Activation");
		mail.setContent(html);

		sendMessage(mail);

	}

	public void sendMessage(Mail mail) {
		System.out.println("EmailService - start : " + new Date());

		Properties mailProps = new Properties();
		mailProps.put("mail.smtp.auth", "true");
		mailProps.put("mail.smtp.starttls.enable", "true");

		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setJavaMailProperties(mailProps);
		sender.setHost("smtp.gmail.com");
		sender.setPort(587);
		sender.setUsername("agilalfrezi@gmail.com");
		sender.setPassword("ogybldllzuvfjapm");

		MimeMessage message = sender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
					StandardCharsets.UTF_8.name());

			helper.setTo(mail.getTo());
			helper.setFrom(mail.getFrom());
			helper.setSubject(mail.getSubject());
			helper.setText(mail.getContent(), true);

			sender.send(message);
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		System.out.println("EmailService - end : " + new Date());
	}

}
