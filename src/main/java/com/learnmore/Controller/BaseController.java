package com.learnmore.Controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.learnmore.DTO.PrincipalDTO;
import com.learnmore.DTO.UserImageDTO;
import com.learnmore.entity.UserImage;
import com.learnmore.entity.Users;
import com.learnmore.entity.constant.AuthProvider;
import com.learnmore.repository.UserRepository;
import com.learnmore.security.UserPrincipal;
import com.learnmore.service.ImageService;

public class BaseController {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ImageService imageService;

	public PrincipalDTO getUserLogin(Principal principal) {
		if (principal != null && principal instanceof Authentication) {
			Users user = userRepository.getByEmail(principal.getName());
			Authentication auth;
			if (user.getProvider().equals(AuthProvider.local)) {
				auth = (UsernamePasswordAuthenticationToken) principal;
			} else {
				auth = (OAuth2AuthenticationToken) principal;
			}

			if (auth != null && auth.getPrincipal() instanceof UserPrincipal) {
				UserPrincipal userlogin = (UserPrincipal) auth.getPrincipal();
				return userlogin.getUserLoginDTO();
			}
		}
		return null;
	}

	@ModelAttribute("profile_image")
	public UserImageDTO userImage(Principal principal) {
		PrincipalDTO userdto = getUserLogin(principal);

		if (userdto == null)
			return null;

		Users users = userRepository.getOne(userdto.getId());
		UserImage data = imageService.findByUserImage(users);
		if (data == null)
			return null;

		UserImageDTO dto = new UserImageDTO();
		dto.setUserId(userdto.getId());
		dto.setFilecode(data.getFileCode());
		dto.setName(users.getName());
		return dto;
	}
}
