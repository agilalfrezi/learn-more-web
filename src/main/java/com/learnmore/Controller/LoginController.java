package com.learnmore.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.learnmore.DTO.LoginDTO;
import com.learnmore.DTO.RegisterDTO;

@Controller
public class LoginController {


	@RequestMapping("/login")
	public String login(Model model) {
		model.addAttribute("login", new LoginDTO());
		model.addAttribute("registerDTO", new RegisterDTO());
		return "login";
	}

}
