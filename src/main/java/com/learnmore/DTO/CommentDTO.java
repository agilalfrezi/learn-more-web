package com.learnmore.DTO;

import java.util.List;

import com.learnmore.entity.CommentReply;

public class CommentDTO {

	private Long articleid;
	private String commentMessage;
	private String name;
	private String email;
	private String website;
	private Long id;
	private List<CommentReply> commentReplies;// remove
	private List<CommentDTO> listreply;
	private boolean message;

	public boolean isMessage() {
		return message;
	}

	public void setMessage(boolean message) {
		this.message = message;
	}

	public List<CommentReply> getCommentReplies() {
		return commentReplies;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCommentReplies(List<CommentReply> commentReplies) {
		this.commentReplies = commentReplies;
	}

	public Long getArticleid() {
		return articleid;
	}

	public void setArticleid(Long articleid) {
		this.articleid = articleid;
	}

	public String getCommentMessage() {
		return commentMessage;
	}

	public void setCommentMessage(String commentMessage) {
		this.commentMessage = commentMessage;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public List<CommentDTO> getListreply() {
		return listreply;
	}

	public void setListreply(List<CommentDTO> listreply) {
		this.listreply = listreply;
	}

}
