package com.learnmore.DTO;

import org.springframework.web.multipart.MultipartFile;

public class PhotoDTO {
	private MultipartFile image;

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}
	
}
