package com.learnmore.DTO;

import java.util.List;

public class ArticleRestDTO {
	private List<ArticleDTO> listArticle;
	private int pagesize;
	private int currentpage;
	public int getCurrentpage() {
		return currentpage;
	}
	public void setCurrentpage(int currentpage) {
		this.currentpage = currentpage;
	}
	public List<ArticleDTO> getListArticle() {
		return listArticle;
	}
	public void setListArticle(List<ArticleDTO> listArticle) {
		this.listArticle = listArticle;
	}
	public int getPagesize() {
		return pagesize;
	}
	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}
}
