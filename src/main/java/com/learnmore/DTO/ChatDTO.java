package com.learnmore.DTO;

public class ChatDTO {
	private String toUser;
	private String name;
	private String content;
	private String imageReceiver;
	private String from;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getImageReceiver() {
		return imageReceiver;
	}

	public void setImageReceiver(String imageReceiver) {
		this.imageReceiver = imageReceiver;
	}

	public String getToUser() {
		return toUser;
	}

	public void setToUser(String toUser) {
		this.toUser = toUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
