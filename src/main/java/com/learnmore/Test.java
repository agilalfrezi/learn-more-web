package com.learnmore;

import java.util.Random;
import java.util.Scanner;
import java.util.Vector;

public class Test {

    String nama2, keterangan, ulang;
    int menu;
    int nim;
    int data;
    double uts;
    double uas;
    double ratarata;
    char Grade;
    String nimLogin;
    String nimPassword;
    String loginAwalNim;
    String loginAwalPassword;
    int menuBaru;
    
    String nama;

    Scanner input = new Scanner(System.in);
    Scanner input2 = new Scanner(System.in);
    Scanner menuBaruInput = new Scanner(System.in);
    Vector< Object[]> KumpulanData = new Vector<>();

    public Test() {
    }

    public double Ratarata() {
        ratarata = ((uts + uas) / 2);
        return ratarata;
    }

    public char Grade() {
        if (ratarata < 50) {
            Grade = 'E';
        } else if (ratarata < 60) {
            Grade = 'D';
        } else if (ratarata < 70) {
            Grade = 'C';
        } else if (ratarata < 80) {
            Grade = 'B';
        } else {
            Grade = 'A';
        }
        return Grade;
    }

    public String keterangan() {
        if (Grade == 'A' || Grade == 'B' || Grade == 'C') {
            keterangan = "Selamat anda lulus";
        }
        return keterangan;
    }

    void input() {
        do {
            System.out.println("Aplikasi siswa");
            System.out.println("Silahkan pilih menu \n" + "1. Aplikasi Ranom \n" + "2. Aplikasi Siswa \n");
            menuBaru = menuBaruInput.nextInt();
            if (menuBaru == 1) {
                System.out.print("Berapa data yang ingin anda masukkan : ");
                data = input.nextInt();
                String[] buah = new String[data];
                int nilaiBaru = data;
                nilaiBaru = nilaiBaru-1;
                int nilaiLain = 1;
                for (int i = 0; i <=nilaiBaru; i++) {
                    System.out.println("Data ke : " + nilaiLain);
                    
                    System.out.print("Masukkan nama : ");
                    nama = input.nextLine();
                    
                    buah[i] = input.next();
                    nilaiLain++;
                }
                System.out.println("Menentukan Random");
                int rnd = new Random().nextInt(buah.length);
                System.out.println("Pemenang adalah : " + buah[rnd]);
            } else {
                if (nimLogin != null || nimPassword != null) {
                    Object[] sebuah = new Object[2];
                    sebuah[0] = nimLogin;
                    sebuah[1] = nimPassword;
                } else {

                    System.out.println("Silahkan daftar masukkan nim dan password anda");

                    System.out.println("nim : ");
                    nimLogin = input2.nextLine();
                    System.out.println("password : ");
                    nimPassword = input2.nextLine();

                    System.out.println("Anda sudah terdaftar silahkan login");
                }

                System.out.println("nim : ");
                loginAwalNim = input2.nextLine();
                System.out.println("password : ");
                loginAwalPassword = input2.nextLine();

                if (loginAwalNim.equals(nimLogin) && loginAwalPassword.equals(nimPassword)) {
                    System.out.println("LOGIN BERHASIL");
                    do {

                        System.out.println("Silahkan pilih menu \n" + "1. Masukkan Data \n" + "2. Tampilkan data \n" + "3. Daftar login \n");

                        System.out.print("Pilih menu ? ");
                        menu = input.nextInt();

                        switch (menu) {
                            case 1:
                                System.out.print("Berapa data yang ingin anda masukkan : ");
                                data = input.nextInt();
                                for (int i = 1; i <= data; i++) {
                                    System.out.println("Data ke : " + i);

                                    System.out.print("Masukkan NIM : ");
                                    nim = input.nextInt();

                                    nama2 = input.nextLine();
                                    System.out.print("Masukkan Nama : ");
                                    nama2 = input.nextLine();

                                    System.out.print("Masukkan Nilai UTS : ");
                                    uts = input.nextDouble();
                                    System.out.print("Masukkan Nilai UAS : ");
                                    uas = input.nextDouble();
                                }
                                break;
                            case 2:
                                Object[] sebuah_data;
                                sebuah_data = new Object[7];
                                sebuah_data[0] = nim;
                                sebuah_data[1] = nama2;
                                sebuah_data[2] = uts;
                                sebuah_data[3] = uas;
                                sebuah_data[4] = Ratarata();
                                sebuah_data[5] = Grade();
                                sebuah_data[6] = keterangan();

                                KumpulanData.addElement(sebuah_data);

                                
                                int temp = 0;
                                System.out.println("=====================================================");
                                for (int i = 1; i <= data; i++) {

                                    sebuah_data = KumpulanData.elementAt(temp);

                                    System.out.print("\n\nData ke-" + (i));

                                    System.out.print("\nNim : " + sebuah_data[0]);

                                    System.out.print("\nNama : " + sebuah_data[1]);

                                    System.out.print("\nNilai uts : " + sebuah_data[2]);

                                    System.out.print("\nNilai uas : " + sebuah_data[3]);

                                    System.out.print("\nRata Rata Nilai : " + sebuah_data[4]);

                                    System.out.print("\nGrade : " + sebuah_data[5]);

                                    System.out.println("\nKeterangan : " + sebuah_data[6]);
                                }
                                System.out.println("=====================================================");
                                break;
                        }

                        System.out.print("Apakah anda ingin \"input data\" atau \"Menampilkan Data\" ? (y/t)? ");
                        ulang = input.next();
                    } while (ulang.equalsIgnoreCase("y"));

                } else {
                    System.out.println("Maaf login gagal");
                }
            }

            System.out.print("Apakah anda ingin keluar program ? (y/t)? ");
            ulang = input.next();
        } while (ulang.equalsIgnoreCase("t"));
    }

    public static void main(String[] args) {
    	Test ss = new Test();
        ss.input();
    }
}
