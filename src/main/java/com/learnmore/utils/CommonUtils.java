package com.learnmore.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

public class CommonUtils {

	public static <E> boolean isNotEmpty(Collection<E> input) {
		if (input != null && input.size() > 0)
			return true;
		return false;
	}

	public static boolean isNotEmpty(String input) {
		if (input != null && input.trim().length() > 0)
			return true;
		return false;
	}

	public static String convertDateToString(Date date, String format) {
		if (date != null && isNotEmpty(format)) {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			return sdf.format(date);
		}
		return null;
	}

	public static boolean isNumber(String number) {

		if (isNotEmpty(number)) {
			String input = number.trim();
			try {
				BigDecimal result = new BigDecimal(input);
				if (result != null) {
					return true;
				}
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	public static Date addHour(Date date, int hours) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, hours);
		return cal.getTime();
	}

	public static String right(String value, int length) {
		if (value.length() < length)
			return value;
		return value.substring(value.length() - length);
	}

	public static boolean isNotEmpty(Map<String, String> message) {
		if (message != null && message.size() > 0)
			return true;
		return false;
	}

	public static String date(Date date) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MMM-dd");

		String dateString = format.format(date);

		String formatAgain = dateString.replace("-", " ");
		return formatAgain;
	}

	public static String removeSpace(String input) {
		if (input != null && input.trim().length() > 0) {
			input.replaceAll("\\s+", "-");
			return input;
		}
		return "";
	}
}
