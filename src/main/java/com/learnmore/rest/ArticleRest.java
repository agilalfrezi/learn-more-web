package com.learnmore.rest;

import java.security.Principal;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.learnmore.Controller.BaseController;
import com.learnmore.DTO.ArticleDTO;
import com.learnmore.DTO.ArticleRestDTO;
import com.learnmore.DTO.CommentDTO;
import com.learnmore.DTO.PrincipalDTO;
import com.learnmore.DTO.ResultDTO;
import com.learnmore.entity.Article;
import com.learnmore.entity.Comment;
import com.learnmore.entity.CommentReply;
import com.learnmore.entity.Users;
import com.learnmore.service.ArticleService;
import com.learnmore.service.CommentService;
import com.learnmore.service.UserService;

@RestController
public class ArticleRest extends BaseController {

	@Autowired
	private ArticleService articleService;
	@Autowired
	private CommentService commentService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/ajax/article/rest", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.GET)
	public ResponseEntity<ArticleRestDTO> productsearchmarketplace(Principal principal,
			@RequestParam(name = "page", required = false) String strpage) {

		Page<Article> page = articleService.getRecentArticle(Integer.valueOf(strpage));
		List<ArticleDTO> listArticleDTOs = articleService.toDTOAricle(page);

		ArticleRestDTO rest = new ArticleRestDTO();
		rest.setListArticle(listArticleDTOs);
		rest.setPagesize(page.getTotalPages());
		return new ResponseEntity<ArticleRestDTO>(rest, HttpStatus.OK);
	}

	@RequestMapping(value = "/ajax/article/reply-comment/rest", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public ResponseEntity<CommentDTO> productsearchmarketplace(@RequestBody String input) {

		JSONObject jsonObject = new JSONObject(input);

		Long replyId = jsonObject.getLong("replyId");
		String replyComment = jsonObject.getString("replyComment");
		String replyName = jsonObject.getString("replyName");
		String replyWebsite = jsonObject.getString("replyWebsite");
		String replyEmail = jsonObject.getString("replyEmail");

		Comment comment = commentService.getById(replyId);
		if (comment != null) {
			CommentReply commentReply = new CommentReply();
			commentReply.setComment(comment);
			commentReply.setCommentMessage(replyComment);
			commentReply.setEmail(replyEmail);
			commentReply.setName(replyName);
			commentReply.setWebsite(replyWebsite);

			commentService.saveCommentReply(commentReply);

		}
		CommentDTO commentDTO = new CommentDTO();
		commentDTO.setMessage(true);

		return new ResponseEntity<CommentDTO>(commentDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/ajax/article/delete-article", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public ResponseEntity<ResultDTO> deleteArticle(@RequestBody String input, Principal principal) {

		JSONObject jsonObject = new JSONObject(input);
		PrincipalDTO principalDTO = getUserLogin(principal);

		Long idArticle = jsonObject.getLong("idArticle");
		System.out.println(jsonObject.toString());
		Users users = userService.findById(principalDTO.getId());

		Article article = articleService.findArticleByIdAndUsers(idArticle, users);
		if (article != null)
			articleService.deleteArticle(article);

		ResultDTO resultDTO = new ResultDTO();
		resultDTO.setSuccess(true);
		return new ResponseEntity<ResultDTO>(resultDTO, HttpStatus.OK);
	}
}
