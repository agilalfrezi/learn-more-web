package com.learnmore.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.learnmore.entity.ImageArticle;
import com.learnmore.entity.UserImage;
import com.learnmore.service.ImageService;

@RestController
public class ImageRestProcessing {

	@Autowired
	private ImageService imageService;

	@GetMapping(value = "/article-image/{filecode}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] articleImage(@PathVariable(value = "filecode") String filecode) {

		byte[] hasil = new byte[] {};
		ImageArticle bannerImage = imageService.findByFilecode(filecode);

		if (bannerImage != null) {
			if (filecode.equals(bannerImage.getFileCode())) {
				String fileLoc = bannerImage.getFileFolder() + File.separator + bannerImage.getFileName();
				FileInputStream fileInputStream = null;
				try {
					fileInputStream = new FileInputStream(new File(fileLoc));
					hasil = IOUtils.toByteArray(fileInputStream);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					if (fileInputStream != null) {
						try {
							fileInputStream.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		return hasil;
	}

	@GetMapping(value = "/user-image/{filecode}", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] userImage(@PathVariable(value = "filecode") String filecode) {

		byte[] hasil = new byte[] {};
		UserImage bannerImage = imageService.findUserImageByFilecode(filecode);

		if (bannerImage != null) {
			if (filecode.equals(bannerImage.getFileCode())) {
				String fileLoc = bannerImage.getFileFolder() + File.separator + bannerImage.getFileName();
				FileInputStream fileInputStream = null;
				try {
					fileInputStream = new FileInputStream(new File(fileLoc));
					hasil = IOUtils.toByteArray(fileInputStream);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} finally {
					if (fileInputStream != null) {
						try {
							fileInputStream.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		return hasil;
	}
}
