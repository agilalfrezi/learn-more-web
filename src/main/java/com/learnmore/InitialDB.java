package com.learnmore;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.learnmore.entity.Article;
import com.learnmore.entity.Category;
import com.learnmore.entity.ImageArticle;
import com.learnmore.entity.SystemParam;
import com.learnmore.entity.Users;
import com.learnmore.entity.UserImage;
import com.learnmore.entity.constant.AuthProvider;
import com.learnmore.entity.constant.SysParam;
import com.learnmore.repository.UserRepository;
import com.learnmore.service.ArticleService;
import com.learnmore.service.CategoryService;
import com.learnmore.service.ExampleImageService;
import com.learnmore.service.ImageService;
import com.learnmore.service.SystemParamService;
import com.learnmore.service.UserService;
import com.learnmore.utils.CommonUtils;

@Component
public class InitialDB {

	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ArticleService articleService;
	@Autowired
	private SystemParamService systemParamService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private ExampleImageService exampleImageService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserRepository userRepository;

	@PostConstruct
	public void init() throws IOException {

		initCategory("Technology");
		initCategory("News");
		initCategory("Tutorial");

		init(SysParam.IMAGE_DIR.toString(), "C:\\development\\upload-image-article",
				"dev : C:\\development\\upload-image-article \n prod : /usr/share/image-digknow/upload-picture");
		init(SysParam.DIG_URL.toString(), "http//localhost/tbxone-web",
				"dev : localhost/tbxone-web \n prod : http//a2cx.com:8080/tbxone");
		init(SysParam.IMAGE_CONTENT.toString(), "C:\\development\\upload-image-article\\content\\",
				"dev : C:\\development\\upload-image-article\\content\\");
		init(SysParam.USR_IMAGE_DIR.toString(), "C:\\development\\upload-image-user\\",
				"dev : C:\\development\\upload-image-user\\");

		createfolder(SysParam.IMAGE_DIR);
		createfolder(SysParam.USR_IMAGE_DIR);

		initUser(1l, "agilAlfrezi@gmail.com", "MALE", "123456789", "Agil", "ADMIN");
		initUser(2l, "rafi@gmail.com", "MALE", "123456789", "Rafi", "USER");

		// initArticle("Contoh Artikel", "Technology", 7l);
		FileUtils.forceMkdir(new File(systemParamService.getSystemParamName("IMAGE_CONTENT").getParamValue()));
	}

	public void initUser(Long id, String email, String gender, String password, String name, String role) {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		Optional<Users> user2 = userRepository.findByEmail(email);
		if (!user2.isPresent()) {
			Users user = new Users();
			UserImage userImage = exampleImageService.initImage("http://www.avajava.com/images/avajavalogo.jpg", email);
			user.setEmail(email);
			user.setPassword(bCryptPasswordEncoder.encode(password));
			user.setGender("MALE");
			user.setEmailVerified(true);
			user.setName(name);
			user.setProviderId("");
			user.setProvider(AuthProvider.local);
			user.setId(id);
			user.setRole(role);
			userRepository.save(user);
			userImage.setUsers(user);
			imageService.saveImageUser(userImage);
		}
	}

	public void initCategory(String categoryName) {
		Category category = categoryService.getCategoryByName(categoryName);
		if (category == null) {
			Category category2 = new Category();
			category2.setCategoryName(categoryName);
			categoryService.saveCategory(category2);
		}
	}

	public void initArticle(String title, String categoryName, Long id) {
		Article article = articleService.getArticleByTitle(title);
		Users user = userService.findById(id);
		if (article == null) {
			ImageArticle image = initImage();
			Category category = categoryService.getCategoryByName(categoryName);
			article = new Article();
			article.setTitle("Contoh Artikel");
			article.setContent("Isi content");
			article.setCreatedDate(new Date());
			article.setUpdatedDate(new Date());
			article.setUsers(user);
			article.setSeen(34567);
			article.setParamUrl("contoh-artikel");
			article.setCategory(category);
			articleService.saveArticle(article);
			image.setArticle(article);
			imageService.saveImage(image);
		}
	}

	private void createfolder(SysParam sysParam) {
		SystemParam data = systemParamService.getSystemParamName(sysParam.toString());
		if (data != null && CommonUtils.isNotEmpty(data.getParamValue())) {
			File newFolder = new File(data.getParamValue());
			newFolder.mkdirs();
		}
	}

	public void init(String code, String value, String description) {
		SystemParam data = systemParamService.getSystemParamName(code);
		if (data == null) {
			data = new SystemParam();
			data.setParamName(code);
			data.setParamValue(value);
			data.setDescription(description);
			systemParamService.saveSystemParam(data);
		}
	}

	public ImageArticle initImage() {

		SystemParam baseImageDir = systemParamService.getSystemParamName("IMAGE_DIR");

		byte[] imageBytes = null;
		URL url = null;
		try {
			url = new URL("http://www.avajava.com/images/avajavalogo.jpg");
			BufferedImage bufferimage = ImageIO.read(url);
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			ImageIO.write(bufferimage, "jpg", output);
			byte[] data = output.toByteArray();
			imageBytes = data;
		} catch (IOException ee) {
			ee.printStackTrace();
		}
		File newFilename = new File(UUID.randomUUID().toString().toLowerCase() + ".jpeg");
		String imageDir = baseImageDir.getParamValue() + File.separator + "ARTIKEL";
		File uploadDir = new File(imageDir);
		if (!uploadDir.exists()) {
			uploadDir.mkdirs();
		}

		try {
			Path path = Paths.get(uploadDir + File.separator + newFilename);
			Files.write(path, imageBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String fileCode = UUID.randomUUID().toString().toLowerCase().replace("-", "");
		ImageArticle image = new ImageArticle();
		image.setContentType("image/jpeg");
		image.setFileFolder(imageDir);
		image.setFileName(newFilename.toString());
		image.setFileCode(fileCode);
		return image;
	}
}
